#ifndef __SPELLCHECK_TRIE_H
#define __SPELLCHECK_TRIE_H

#include <linkedlist.h>
#include <stdlib.h>

struct trie {
    struct ll root;
};

struct trie new_trie(char *seed);
void add_word_trie(struct trie *dictionary, char *word, ssize_t len);
int has_word_trie(struct trie *dictionary, const char *word, ssize_t len);
void print_trie(struct trie *dictionary);

#endif
