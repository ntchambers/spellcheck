#ifndef __SPELLCHECK_NODE_H
#define __SPELLCHECK_NODE_H

#include <linkedlist.h>

struct node {
    struct node *prev;
    struct node *next;
    char index;
    struct ll *next_list;
    int is_end;
};

struct node *new_node(char index);
struct node *insert_node(struct node *entry, char index);

#endif
