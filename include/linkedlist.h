#ifndef __SPELLCHECK_LINKEDLIST_H
#define __SPELLCHECK_LINKEDLIST_H

#include <node.h>

struct ll {
    struct node *list;
    struct node *head;
    struct node *tail;
};

struct ll new_ll();
struct node *insert_ll(struct ll *list, char index);

#endif
