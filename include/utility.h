#ifndef __SPELLCHECK_UTILITY_H
#define __SPELLCHECK_UTILITY_H

#include <stdlib.h>

void die_util(const char *fmt, ...);
ssize_t is_file_util(const char *filename);
char *read_file_util(const char *filename, size_t size);

#endif
