#include <ctype.h>
#include <stdio.h>
#include <trie.h>

struct trie new_trie(char *seed) {
    struct trie new = { .root = new_ll() };
    char *start = seed;
    ssize_t len = 0;

    for(char *letter = seed; *letter; ++letter) {
        if(*letter == '\n') {
            add_word_trie(&new, start, len);
            len = 0;
            start = letter + 1;
        } else {
            ++len;
        }
    }

    return new;
}

void add_word_trie(struct trie *dictionary, char *word, ssize_t len) {
    ssize_t depth = 0;
    struct ll *current = &(dictionary->root);

    while(depth < len) {
        int index_found = 0;

        for(struct node *iter = current->head; index_found == 0 && iter != NULL; iter = iter->next) {
            if(*word == iter->index) {
                index_found = 1;
                current = iter->next_list;
                ++depth, ++word;

                if(depth == len) {
                    iter->is_end = 1;
                }
            }
        }

        if(!index_found) {
            struct node *entry = insert_ll(current, *word);
            current = entry->next_list;
            ++depth, ++word;

            if(depth == len) {
                entry->is_end = 1;
            }
        }
    }
}

int has_word_trie(struct trie *dictionary, const char *word, ssize_t len) {
    if(len == 0) {
        return 0;
    }

    ssize_t depth = 0;
    struct ll *current = &(dictionary->root);
    struct node *end = NULL;

    while(depth < len) {
        int index_found = 0;

        for(struct node *iter = current->head; !index_found && iter != NULL; iter = iter->next) {
            if(*word == iter->index) {
                index_found = 1;
                current = iter->next_list;
                end = iter;
                ++depth, ++word;
            }
        }

        if(!index_found) {
            return 0;
        }
    }

    return end->is_end;
}

static void print_list(struct ll *list, int depth) {
    for(struct node *iter = list->head; iter != NULL; ++iter) {
        for(int count = 0; count < depth; ++count) {
            fputc('*', stdout);
        }

        if(isprint(iter->index)) {
            printf(" %c\n", iter->index);
        } else {
            printf(" %d\n", (int) iter->index);
        }

        print_list(iter->next_list, depth + 1);
    }
}

void print_trie(struct trie *dictionary) {
    print_list(&(dictionary->root), 1);
}
