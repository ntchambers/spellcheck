#include <node.h>
#include <stdlib.h>

struct node *new_node(char index) {
    struct node *new = malloc(sizeof(struct node));
    new->next = NULL;
    new->prev = NULL;
    new->index = index;
    new->next_list = malloc(sizeof(struct ll));
    new->next_list->list = NULL;
    new->next_list->head = NULL;
    new->next_list->tail = NULL;
    new->is_end = 0;
    return new;
}

struct node *insert_node(struct node *entry, char index) {
    struct node *insert = new_node(index);
    insert->prev = entry;

    if(entry != NULL) {
        insert->next = entry->next;
        entry->next = insert;
    }

    return insert;
}
