#include <stdio.h>
#include <string.h>
#include <trie.h>
#include <unistd.h>
#include <utility.h>

int main(int argc, char **argv) {
    int arg = opterr = 0;
    int silent = 0;
    char *dictionary_buf = "/usr/share/dict/words";
    ssize_t size = 0;

    while((arg = getopt(argc, argv, "sd:")) != -1) {
        if(arg == 's') {
            silent = 1;
        } else if(arg == 'd') {
            dictionary_buf = optarg;
        } else if(arg == '?') {
            fprintf(stderr, "unrecognized option '-%c'\n", optopt);
            return 1;
        } else {
            fprintf(stderr, "you should not see this.\n");
            fprintf(stderr, "please contact your local priest/unix sysadmin.\n");
            return 1;
        }
    }

    if(!(size = is_file_util(dictionary_buf))) {
        die_util("the dictionary %s could not be found", dictionary_buf);
    }

    dictionary_buf = read_file_util(dictionary_buf, size);
    struct trie dictionary = new_trie(dictionary_buf);
//    print_trie(&dictionary);

    for(int count = optind; count < argc; ++count) {
        printf("%s: %s\n", argv[count], has_word_trie(&dictionary, argv[count], strlen(argv[count])) ? "correct" : "wrong");
    }

    return 0;
}
