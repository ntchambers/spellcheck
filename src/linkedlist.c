#include <linkedlist.h>
#include <stdlib.h>

struct ll new_ll() {
    struct ll new;
    new.list = NULL;
    new.head = NULL;
    new.tail = NULL;
    return new;
}

struct node *insert_ll(struct ll *list, char index) {
    list->list = insert_node(list->tail, index);
    list->tail = list->list;

    if(list->head == NULL) {
        list->head = list->list;
    }

    return list->tail;
}
