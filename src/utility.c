#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <utility.h>

void die_util(const char *fmt, ...) {
    va_list args;
    int cached = errno;

    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);

    if(fmt[strlen(fmt) - 1] == ':') {
        fprintf(stderr, " %s", strerror(cached));
    }

    fputc('\n', stderr);
    exit(cached);
}

ssize_t is_file_util(const char *filename) {
    struct stat buf;
    return stat(filename, &buf) < 0 ? 0 : buf.st_size;
}

char *read_file_util(const char *filename, size_t size) {
    int fd = open(filename, O_RDONLY);

    if(fd < 0) {
        return NULL;
    }

    char *buf = malloc(size);
    ssize_t bytes_read = read(fd, buf, size);

    while(bytes_read < size) {
        ssize_t bytes = read(fd, buf + bytes_read, size - bytes_read);
        bytes_read += bytes;

        if(!bytes) {
            break;
        }
    }

    close(fd);
    return buf;
}
